import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: "./component/main/main.module#MainModule" },
  { path: 'register', loadChildren: "./component/pages/register/register.module#RegisterModule" },
  { path: 'login', loadChildren: "./component/pages/login/login.module#LoginModule" },
  { path: 'userprofile', loadChildren: "./component/pages/userprofile/userprofile.module#UserprofileModule" },
  { path: 'management', loadChildren: "./component/pages/management/management.module#ManagementModule" },
  { path: 'cart', loadChildren: "./component/pages/cart/cart.module#CartModule" },
  { path: 'history', loadChildren: "./component/pages/history/history.module#HistoryModule" },
  { path: 'search', loadChildren: "./component/pages/search/search.module#SearchModule" },
  { path: 'report', loadChildren: "./component/pages/report/report.module#ReportModule" },
  { path: 'orders', loadChildren: "./component/pages/orders/orders.module#OrdersModule" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
