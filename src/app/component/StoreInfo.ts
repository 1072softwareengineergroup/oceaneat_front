import { CustomerInfo } from './UserInfo';
import { AppComponent } from '../app.component';

export class StoreInfo {
    storeName : string
    phone : string
    location : string
    principal : string
    email : string
    menu : object
    picture : string

    static callTime = 0;

    constructor(
        storeName : string = undefined, phone : string = undefined, location : string = undefined,
        principal : string = undefined, email : string = undefined, picture : string = undefined, 
        menu : object = undefined, public position: object = {lat: 25.15, lng: 121.77}, public work_uid = null) {
        if (menu !== undefined) this.menu = menu;
        else this.menu = {};

        this.storeName = storeName
        this.phone = phone
        this.location = location
        this.principal = principal
        this.email = email
        this.picture = picture
        
        if (!work_uid) this.setWorkUid();
    }

    clone() : StoreInfo {
        return new StoreInfo(
            this.storeName,
            this.phone,
            this.location,
            this.principal,
            this.email,
            this.picture,
            Object.create(this.menu)
        )
    }

    setWorkUid(): void {
        StoreInfo.callTime++;
        const str = JSON.stringify(this);
        let result = str.charCodeAt(0);
        for (let i = 1; i < str.length; ++i) result = 31 * result + str.charCodeAt(i);
        this.work_uid = result;
    }

    static createFromJsonObj(json: object, owner: CustomerInfo): StoreInfo {
        const pos = json['lacation'] ? json['lacation'].split(", ") : [25.15, 121.77];
        return new StoreInfo(
            json['restaurant_name'],
            json['phone_number'],
            json['address'],
            owner.userName,
            json['mail_address'],
            AppComponent.API_URL + 'api/Restaurant/get_picture/?rid=' + json['restaurant_id'],
            undefined, // menu
            {lat: pos[0], lng: pos[1]}, // position
            json['restaurant_id']
        )
    }

    static createFromJsonObjNoOwner(json: object): StoreInfo {
        const pos = json['lacation'] ? json['lacation'].split(", ") : [25.15, 121.77];
        return new StoreInfo(
            json['restaurant_name'],
            json['phone_number'],
            json['address'],
            '',
            json['mail_address'],
            AppComponent.API_URL + 'api/Restaurant/get_picture/?rid=' + json['restaurant_id'],
            undefined, // menu
            {lat: pos[0], lng: pos[1]}, // position
            json['restaurant_id']
        )
    }

    static createBlank(): StoreInfo {
        return new StoreInfo(
            '',
            '',
            '',
            '',
            '',
            '',
            {}
        );
    }

    getFormatJsonObj(): object {
        return {
            address: this.location,
            lacation: `${this.position['lat']}, ${this.position['lng']}`,
            mail_address: this.email,
            phone_number: this.phone,
            restaurant_name: this.storeName,
            restaurant_id: this.work_uid
        };
    }
}