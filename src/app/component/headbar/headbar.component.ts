import { Component, OnInit, Input } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { OrdersService } from '../pages/orders/orders.service';
import { SwPush } from '@angular/service-worker';
import { ReloadOrdersService } from '../pages/orders/reload-orders.service';

@Component({
  selector: 'app-headbar',
  templateUrl: './headbar.component.html',
  styleUrls: ['./headbar.component.css']
})
export class HeadbarComponent implements OnInit {
  @Input() showLogin = true;
  init : boolean = true;
  showNav : boolean = false;

  constructor(private swPush: SwPush, public orderService: OrdersService, private reloadService: ReloadOrdersService) { }

  ngOnInit() {
    AppComponent.CallAfterInit.push(() => {
      if (this.visitNotValid()) this.redirect();
    });

    if (AppComponent.isDelivery) {
      this.swPush.requestSubscription({
        serverPublicKey: AppComponent.VAPID_PUBLIC_KEY
      })
      .then(sub => sub.toJSON())
      .then(sub => this.orderService.addPushSubscriber(sub).subscribe())
      .catch(err => console.error("Could not subscribe to notifications", err));
      this.swPush.notificationClicks.subscribe(payload => {
        if (payload.action === 'ShowOrderPage') {
          if (!window.location.href.includes('/orders')) window.open('/orders', '_self');
          else this.reloadService.reload();
        }
      });
    } else {
      this.swPush.subscription.subscribe((sub) => {
        this.orderService.unsubscribe(sub);
      });
    }
    AppComponent.excute();
  }

  visitNotValid(): boolean {
    const href = window.location.href;
    if (this.isCustomer) {
      if (
        href.includes('/register') ||
        href.includes('/login') ||
        href.includes('/orders') ||
        href.includes('/management')
      ) return true;
    } else if (this.isDelivery) {
      if (
        href.includes('/register') ||
        href.includes('/login') ||
        href.includes('/cart') ||
        href.includes('/management')
      ) return true;
    } else if (this.isRestaurant) {
      if (
        href.includes('/register') ||
        href.includes('/login') ||
        href.includes('/cart') ||
        href.includes('/history') ||
        href.includes('/orders')
      ) return true;
    } else {
      // not login
      if (
        href.includes('/history') ||
        href.includes('/management') ||
        href.includes('/cart') ||
        href.includes('/userprofile') ||
        href.includes('/orders')
      ) return true;
    }
    return false;
  }

  redirect() {
    if (!this.isRestaurant) window.open('/management', '_self');
    window.open('/', '_self');
  }

  get isLogin(): boolean { return AppComponent.isLogin }
  get isCustomer(): boolean { return AppComponent.isCustomer }
  get isDelivery(): boolean { return AppComponent.isDelivery }
  get isRestaurant(): boolean { return AppComponent.isRestaurant }
  get isQualified(): boolean { return AppComponent.isQualified }

  toggleShowNav() : void {
    this.showNav = !this.showNav;
    this.init = false;
  }

  logout() : void {
    fetch(AppComponent.API_URL + 'logout/', {
      mode: 'cors',
      credentials: 'include',
      method: 'POST'
    }).then(res => {
      AppComponent.testLogin();
      window.open(window.location.href, '_self');
    });
  }

  toggleUser(): void {
    fetch(AppComponent.API_URL + 'api/Customer/switch_mode/', {
      method: 'POST',
      body: JSON.stringify({customer_id: AppComponent.UserId}),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => res.json()).then(data => {
      console.log(data);
      AppComponent.testLogin();
      alert(`切換使用者身分為 "${this.getSwitchType()}", 將重新整理!!`);
      setTimeout(() => {
        window.open(window.location.href, '_self');
      }, 300);
    });
  }

  getUserType(): string {
    switch (AppComponent.userType) {
      case 0:
        return '使用者';
      case 1:
        return '外送員';
    }
  }

  getSwitchType(): string {
    switch (AppComponent.userType) {
      case 0:
        return '外送員';
      case 1:
        return '使用者';
    }
  }
}
