import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HeadbarComponent } from './headbar.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        HeadbarComponent
    ],
    exports: [
        HeadbarComponent
    ]
})
export class HeadbarModule { }