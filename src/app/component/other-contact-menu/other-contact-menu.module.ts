import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { OtherContactMenuComponent } from './other-contact-menu.component';
import { DisplayTableComponent } from './display-table/display-table.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        OtherContactMenuComponent,
        DisplayTableComponent
    ],
    exports: [
        OtherContactMenuComponent,
        DisplayTableComponent
    ]
})
export class OtherContactMenuModule { }