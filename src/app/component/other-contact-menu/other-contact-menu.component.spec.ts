import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherContactMenuComponent } from './other-contact-menu.component';

describe('OtherContactMenuComponent', () => {
  let component: OtherContactMenuComponent;
  let fixture: ComponentFixture<OtherContactMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherContactMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherContactMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
