import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-display-table',
  templateUrl: './display-table.component.html',
  styleUrls: ['./display-table.component.css']
})

export class DisplayTableComponent implements OnInit {
  @Input() contactDict : object;
  @Output() deleteEvent = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  showing() : boolean {
    for (let attr in this.contactDict) {
      if (attr !== undefined) return true;
    }
    return false;
  }

  delete(key: string) {
    let newDict = {};
    for (let attr in this.contactDict) {
      if (attr === key) continue;
      newDict[attr] = this.contactDict[attr];
    }
    this.deleteEvent.emit(newDict);
    return false;
  }
}
