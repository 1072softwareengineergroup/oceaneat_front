import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-other-contact-menu',
  templateUrl: './other-contact-menu.component.html',
  styleUrls: ['./other-contact-menu.component.css']
})

export class OtherContactMenuComponent implements OnInit {
  @Input() contactDict : object = {};
  @Input() editable : boolean = true;
  @Output() changeEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  addContact() : void {
    let selectedVal : string = (<HTMLInputElement>document.getElementById('selectMenu')).value;
    let inputVal : string = (<HTMLInputElement>document.getElementById('contactValue')).value;;

    if (selectedVal.trim().length === 0 || inputVal.trim().length === 0) return;

    this.contactDict[selectedVal] = inputVal;
    this.saveData();
  }

  deleteContactEvent(obj: object) {
    this.contactDict = obj;
    this.saveData();
  }

  getContact() : any {
    return JSON.stringify(this.contactDict);
  }

  hasValue() : boolean {
    for (let attr in this.contactDict) {
      if (attr !== undefined) return true;
    }
    return false;
  }

  saveData() {
    this.changeEvent.emit();
  }
}
