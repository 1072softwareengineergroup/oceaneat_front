/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild } from '@angular/core';
import { CartComponent } from '../pages/cart/cart.component';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-float-cart',
  templateUrl: './float-cart.component.html',
  styleUrls: ['./float-cart.component.css']
})
export class FloatCartComponent implements OnInit {

  constructor() { }
  static isShowing = false;
  hasLogin = AppComponent.isLogin;
  @ViewChild(CartComponent) cart: CartComponent;
  curLocLen = 0;

  ngOnInit() {
    const modal = document.querySelector('#cartModal') as HTMLDivElement;
    const span = document.querySelector('.close') as HTMLSpanElement;

    window.onclick = (event) => {
      if (event.target === modal) {
        modal.style.display = 'none';
        FloatCartComponent.isShowing = false;
      }
    };
    span.onclick = () => {
      modal.style.display = 'none';
      FloatCartComponent.isShowing = false;
    };
  }
  update(): void {
    if (FloatCartComponent.isShowing) { return; }
    if (this.curLocLen === localStorage.length) { return; }
    this.cart.onLoadUpdate();
    this.curLocLen = localStorage.length;
  }

  btnClick() {
    const modal =  document.querySelector('#cartModal') as HTMLDivElement;
    modal.style.display = 'block';
    FloatCartComponent.isShowing = true;
  }
}
