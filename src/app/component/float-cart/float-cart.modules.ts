import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FloatCartComponent } from './float-cart.component';
import { CartModule } from '../pages/cart/cart.module';
import { LoginModule } from '../pages/login/login.module';

@NgModule({
    imports: [
        CommonModule,
        CartModule,
        LoginModule
    ],
    declarations: [
        FloatCartComponent
    ],
    exports: [
        FloatCartComponent
    ]
})
export class FloatCartModule { }