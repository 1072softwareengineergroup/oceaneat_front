/// <reference types="@types/googlemaps" />
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { OrderInfo, OrderItem } from '../../OrderInfo';
import { StoreInfo } from '../../StoreInfo';
import { OrderDisplayComponent } from '../../order-display/order-display.component';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @Input() showHeader = true;
  @ViewChild(OrderDisplayComponent) display: OrderDisplayComponent;
  orderInfo: OrderInfo = new OrderInfo();

  private preRestaurantList: number[] = [];
  private directionsService: google.maps.DirectionsService = null;

  constructor() {
  }

  ngOnInit() {
    this.onLoadUpdate();

    window.onbeforeunload = () => {
      this.updateObject();
    };
  }
  
  mapReady(map: google.maps.Map) {
    this.directionsService = new google.maps.DirectionsService();
  }

  onLoadUpdate(): void {
    this.updateCart();
    this.updateObject();
    this.updateCart();
  }

  updateCart(): void {
    this.orderInfo = new OrderInfo();
    for (let i = localStorage.length - 1; i >= 0; --i) {
      if (localStorage.key(i).startsWith('cart-add-')) {
        const key = localStorage.key(i);
        const order = JSON.parse(localStorage.getItem(key));
        const orders: OrderItem[] = [];
        for (const item of order.orders) {
          orders.push(new OrderItem(item.menuItem, item.count));
        }
        this.addToCart(order.store, orders);
      }
    }
    this.setDistance();
  }

  addToCart(store: StoreInfo, items: OrderItem[]) {
    if (store === undefined || items === undefined) {
      return;
    }
    items.forEach(item => this.orderInfo.addToOrder(store, item));
  }

  clearCart(): void {
    this.orderInfo = new OrderInfo();
    for (let i = localStorage.length - 1; i >= 0; --i) {
      if (localStorage.key(i).startsWith('cart-add-')) {
        const key = localStorage.key(i);
        localStorage.removeItem(key);
      }
    }
  }

  updateObject(): void {
    if (this.orderInfo.menuItems.size === 0) {
      return;
    }
    let jsonList = [];
    let index = 0;
    for (const e of this.orderInfo.menuItems.entries()) {
      jsonList.push(JSON.stringify({store: e[0], orders: e[1]}));
    }
    for (let i = localStorage.length - 1; i >= 0; --i) {
      let add = true;
      if (localStorage.key(i).startsWith('cart-add-')) {
        const key = localStorage.key(i);
        const info = localStorage.getItem(key);
        for (const o of jsonList) {
          if (JSON.parse(info).store.work_uid === JSON.parse(o).store.work_uid) {
            add = false;
            break;
          }
        }
        if (add) {
          jsonList.push(info);
        }
      }
    }
    for (let i = localStorage.length - 1; i >= 0; --i) {
      if (localStorage.key(i).startsWith('cart-add-')) {
        const key = localStorage.key(i);
        localStorage.removeItem(key);
      }
    }

    let mergedMap = new Map<number, object>();
    for (const str of jsonList) {
      if (mergedMap.get(JSON.parse(str).store.work_uid)) {
        for (let obj of JSON.parse(str).orders) mergedMap.get(JSON.parse(str).store.work_uid)['orders'].push(obj);
      } else {
        let obj = JSON.parse(str);
        mergedMap.set(obj.store.work_uid, obj);
      }
    }
    jsonList = [];
    for (const obj of mergedMap.values())
      jsonList.push(JSON.stringify(obj));
    for (const str of jsonList) {
      if (JSON.parse(str).orders.length > 0) {
        localStorage.setItem('cart-add-CART-' + index++, str);
      }
    }
  }

  setDistance(): void {
    if (this.directionsService === null) return;
    let hasModify: boolean = false;
    for (const store of this.orderInfo.getKeys()) {
      if (this.preRestaurantList.includes(store.work_uid)) continue;
      hasModify = true;
      break;
    }
    if (!hasModify && this.preRestaurantList.length > 0) return;
    
    this.preRestaurantList = [];
    const lat = AppComponent.CurLocation['position']['lat'];
    const lng = AppComponent.CurLocation['position']['lng'];

    let restaurants: google.maps.DirectionsWaypoint[] = [];
    for (let store of this.orderInfo.getKeys()) {
      restaurants.push({
        location: new google.maps.LatLng(store.position['lat'], store.position['lng'])
      });
      this.preRestaurantList.push(store.work_uid);
    }

    let request: google.maps.DirectionsRequest = {
      origin: { lat: lat, lng: lng },
      waypoints: restaurants,
      destination: { lat: lat, lng: lng },
      travelMode: google.maps.TravelMode.DRIVING
    };
    this.directionsService.route(request, (result, status) => {
      if (status.toString() === 'OK') {
        this.orderInfo.distance = result.routes[0].legs[0].distance.value * 0.001;
        this.orderInfo.getDeliveryProfit();
      } else { console.log(status); };
    });
  }

  checkOut() : void {
    if (this.orderInfo.menuItems.size === 0) return;
    if (AppComponent.CurLocation === null) {
      alert('請輸入地址欄位!!');
      return;
    }
    this.updateObject();
    const customerId = AppComponent.UserId;
    const curLocation = AppComponent.CurLocation;
    const destination = this.orderInfo.location = curLocation['address'];
    this.orderInfo.position = curLocation['position'];

    fetch(AppComponent.API_URL + 'api/Order/create_order/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        customer_id: customerId,
        order_items: JSON.stringify(this.orderInfo.getJson()),
        dest: destination,
        price: this.orderInfo.getFinalPrice()
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      if (res.status === 200) return res.json();
      console.error(res);
      console.error(res.json());
      return null;
    })
    .then(data => {
      if (data === null) return;
      this.clearCart();
      alert('成功建立訂單!!');
      fetch(AppComponent.API_URL + 'send_push/');
      window.open('/history', '_self');
    });
  }
}
