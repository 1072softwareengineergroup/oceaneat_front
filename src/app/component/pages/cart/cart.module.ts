import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { HeadbarModule } from '../../headbar/headbar.module';
import { CartRoutingModule } from './cart-routing.module';
import { OrderDisplayModule } from '../../order-display/order-display.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        CartRoutingModule,
        HeadbarModule,
        OrderDisplayModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDKNCospDzFb7ZTxzvz3vlTojc7Wz6rLXQ',
            libraries: ["directions"]
        })
    ],
    declarations: [
        CartComponent
    ],
    exports: [
        CartComponent
    ]
})
export class CartModule { }