import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HeadbarModule } from '../../headbar/headbar.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        HeadbarModule
    ],
    declarations: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ]
})
export class LoginModule { }