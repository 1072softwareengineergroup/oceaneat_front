import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import * as sha1 from 'js-sha1';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() showHeader = true;

  constructor() { }

  ngOnInit() {
  }

  login() : void {
    let email = (<HTMLInputElement>document.querySelector('#email')).value
    let password = (<HTMLInputElement>document.querySelector('#pass')).value

    fetch(AppComponent.API_URL + 'login/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        username: email,
        password: sha1(password)
      }),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => {
      if (res.status === 200) return res.json();
      else {
        if (this.showHeader) {
          alert('帳號或密碼錯誤!!')
        } else {
          console.log('帳號或密碼錯誤');
        }
        return null;
      }
    }).then(data => {
      if (data === null) return;
      if (data['status'] === true) {
        if (this.showHeader) {
          AppComponent.testLogin(true);
          alert('登入成功!!');
        } else {
          AppComponent.testLogin();
          alert('登入成功!!');
          window.open(window.location.href, '_self');
        }
      } else {
        if (this.showHeader) alert(data['message']);
        AppComponent.testLogin();
        setTimeout(() => {
          window.open(window.location.href, '_self');
        }, 350);
      }
    })
  }
}
