import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrderInfo } from 'src/app/component/OrderInfo';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.css']
})
export class OrderCardComponent implements OnInit {
  @Input() orderInfo: OrderInfo;
  @Output() showInfoEvent = new EventEmitter<string>();
  @Output() orderEvent = new EventEmitter<OrderInfo>();
  showInfo = false;

  constructor() { }

  ngOnInit() {
  }

  toggleInfo(option: boolean = null) {
    if (option === null && !this.showInfo) if (option !== this.showInfo) this.showInfoEvent.emit(this.orderInfo.orderId);
    this.showInfo = option === null ? !this.showInfo : option;
    return false;
  }
  highLight(): OrderCardComponent {
    const div = document.querySelector(`#orderCard-${this.orderInfo.orderId}`) as HTMLDivElement;

    div.classList.add('highLight');
    setTimeout(() => {div.classList.remove('highLight');}, 400)

    return this;
  }

  getOrder() {
    if (!confirm('確認接單？')) return;

    this.orderEvent.emit(this.orderInfo);

    fetch(AppComponent.API_URL + 'api/Order/accept_order/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({ delivery_id: AppComponent.UserId, order_id: this.orderInfo.orderId }),
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(res => res.status === 200 ? res.json() : console.log)
    .then(data => data['status'] ? console.log : console.log);
  }
}
