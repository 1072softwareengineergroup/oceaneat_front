import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AppComponent } from 'src/app/app.component';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {

  constructor(private http: HttpClient) {}

  addPushSubscriber(sub: any) {
    const browser = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)[0].toLowerCase();
    const data = {
        status_type: 'subscribe',
        subscription: sub,
        group: "delivery",
        browser: browser,
    };
    return this.http.post(
      AppComponent.API_URL + 'webpush/save_information/', 
      JSON.stringify(data)
    );
  }

  send() {
      return this.http.post(AppComponent.API_URL + 'send_push/', null);
  }

  unsubscribe(sub: any) {
    const browser = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)[0].toLowerCase();
    const data = {
        status_type: 'unsubscribe',
        subscription: sub,
        group: "delivery",
        browser: browser,
    };
    return this.http.post(
      AppComponent.API_URL + 'webpush/save_information/', 
      JSON.stringify(data)
    );
  }
}
