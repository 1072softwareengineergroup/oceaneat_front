import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';
import { HeadbarModule } from '../../headbar/headbar.module';
import { OrderCardComponent } from './order-card/order-card.component';
import { AgmCoreModule } from '@agm/core';
import { OrderGoingComponent } from './order-going/order-going.component';

@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    HeadbarModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDKNCospDzFb7ZTxzvz3vlTojc7Wz6rLXQ',
      libraries: ["directions"]
    })
  ],
  declarations: [
    OrdersComponent,
    OrderCardComponent,
    OrderGoingComponent
  ],
  exports: [
    OrdersComponent
  ]
})
export class OrdersModule { }