import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderGoingComponent } from './order-going.component';

describe('OrderGoingComponent', () => {
  let component: OrderGoingComponent;
  let fixture: ComponentFixture<OrderGoingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderGoingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderGoingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
