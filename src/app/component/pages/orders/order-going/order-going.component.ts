import { Component, OnInit, Input } from '@angular/core';
import { OrderInfo } from 'src/app/component/OrderInfo';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-order-going',
  templateUrl: './order-going.component.html',
  styleUrls: ['./order-going.component.css']
})
export class OrderGoingComponent implements OnInit {
  @Input() orderInfo: OrderInfo;
  @Input() distInfo: object;
  distance: string = null;
  duration: string = null;

  constructor() { }

  ngOnInit() {
    this.updateDistInfo(this.distInfo);
  }

  updateDistInfo(distInfo: object) {
    this.distInfo = distInfo;
    if (this.distInfo) {
      this.distance = this.distInfo['distance'];
      this.duration = this.distInfo['duration'];
    }
  }

  finishOrder() {
   window.open('/history?id=' + this.orderInfo.orderId, '_self'); 
  }
}
