/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { OrderInfo } from '../../OrderInfo';
import { AgmMarker } from '@agm/core';
import { OrderCardComponent } from './order-card/order-card.component';
import { AppComponent } from 'src/app/app.component';
import { OrderGoingComponent } from './order-going/order-going.component';
import { ReloadOrdersService } from './reload-orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  @ViewChildren(OrderCardComponent) cards: QueryList<OrderCardComponent>;
  @ViewChildren(AgmMarker) markers: QueryList<AgmMarker>;
  @ViewChild(OrderGoingComponent) orderGoing: OrderGoingComponent;
  orderList: OrderInfo[] = [];
  isOrderGoing = false;
  onGoingOrder: OrderInfo = null;
  onGoingOrderDistInfo: object = null;
  lazyLodingOrder: OrderInfo = null;

  lat = 25.15044;
  lng = 121.7755731;
  zoom = 16;

  private myLat: number;
  private myLng: number;

  private googleMap: google.maps.Map = null;
  private directionsService: google.maps.DirectionsService = null;
  private directionsDisplay: google.maps.DirectionsRenderer = null;

  constructor(private reloadService: ReloadOrdersService) { }

  ngOnInit() {
    fetch(AppComponent.API_URL + 'api/Order/query_order_by_delivery/?delivery_id=' + AppComponent.UserId, { method: 'GET' })
    .then(res => res.json()).then(data => {
      for (const order of data['orders']) {
        const tmpInfo = OrderInfo.createFromObj(order);
        if (tmpInfo.isFinished() || tmpInfo.isError()) continue;
        if (tmpInfo.isPending() && !tmpInfo.isGoingOrPendingMe()) continue;
        this.lazyLodingOrder = tmpInfo; // lazyLoadingOrder -> ongoing order
        break;
      }
      this.reloadService.setCallBack(this.lazyLodingOrder ? () => {} : this.loadOrders);
    });
    this.loadOrders();
  }

  loadOrders() {
    fetch(AppComponent.API_URL + 'api/Order/query_unassigned_order/', {
      method: 'GET'
    }).then(res => res.json()).then(data => {
      this.orderList = [];
      for (let o of data['orders']) this.orderList.push(OrderInfo.createFromObj(o));
    });
  }

  mapReady(map: google.maps.Map) {
    this.googleMap = map;
    this.setCurrentLocation();
    if (this.lazyLodingOrder) this.orderEvent(this.lazyLodingOrder);
  }

  setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.myLat = position.coords.latitude;
        this.myLng = position.coords.longitude;
      });
    } else {
      this.myLat = this.lat = AppComponent.CurLocation['lat'];
      this.myLng = this.lng = AppComponent.CurLocation['lng'];
    }
    if (!this.myLat || !this.myLng) {
      this.myLat = this.lat = 25.15044;
      this.myLng = this.lng = 121.7755731;
    }
  }
  markerClick(marker: AgmMarker) {
    const id = marker.title;
    this.cards.toArray().forEach(c => c.toggleInfo(false));
    this.cards.toArray().filter(c => c.orderInfo.orderId === id)[0].highLight().toggleInfo(true);
  }

  showInfoEvent(id: string) {
    this.markers.toArray().forEach(m => m.infoWindow.toArray()[0].close());
    this.markers.toArray().filter(m => m.title === id)[0].infoWindow.toArray()[0].open();
  }

  orderEvent(order: OrderInfo) {
    this.isOrderGoing = true;
    this.onGoingOrder = order;

    this.loadGoogleMapLibries();
    this.directionsDisplay.setMap(this.googleMap);

    let restaurants: google.maps.DirectionsWaypoint[] = [];
    for (let store of order.getKeys()) {
      restaurants.push({
        location: new google.maps.LatLng(store.position['lat'], store.position['lng'])
      });
    }
    
    let request: google.maps.DirectionsRequest = {
      origin: { lat: this.myLat, lng: this.myLng },
      waypoints: restaurants,
      destination: { lat: this.onGoingOrder.position['lat'], lng: this.onGoingOrder.position['lng'] },
      travelMode: google.maps.TravelMode.DRIVING
    };

    this.directionsService.route(request, (result, status) => {
      if (status.toString() === 'OK') {
        // 回傳路線上每個步驟的細節
        let steps = result.routes[0].legs[0].steps;
        const map = this.googleMap;
        let markers = [];
        let infowindows = [];
        steps.forEach((e, i) => {
          // 加入地圖標記
          markers[i] = new google.maps.Marker({
          position: { lat: e.start_location.lat(), lng: e.start_location.lng() },
          map: map,
          label: { text: i + '', color: "#fff" }
          });
          // 加入資訊視窗
          infowindows[i] = new google.maps.InfoWindow({
          content: e.instructions
          });
          // 加入地圖標記點擊事件
          markers[i].addListener('click', function () {
            if(infowindows[i].anchor){
                infowindows[i].close();
            }else{
                infowindows[i].open(map, markers[i]);
            }
          });
        });
        this.directionsDisplay.setDirections(result);
        this.onGoingOrderDistInfo = {
          distance: result.routes[0].legs[0].distance.text,
          duration: result.routes[0].legs[0].duration.text
        };
        this.orderGoing.updateDistInfo(this.onGoingOrderDistInfo);
      } else { console.log(status); }
    });
  }
  
  markerDragEnd(coords: object) {
    coords = coords['coords'];
    if (!coords || !coords['lat'] || !coords['lng']) return;
    this.myLat = coords['lat'];
    this.myLng = coords['lng'];
  }

  loadGoogleMapLibries() {
    if (this.directionsService === null) this.directionsService = new google.maps.DirectionsService();
    if (this.directionsDisplay === null) this.directionsDisplay = new google.maps.DirectionsRenderer();
  }
}
