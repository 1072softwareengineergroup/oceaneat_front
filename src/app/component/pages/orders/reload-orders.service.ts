import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class ReloadOrdersService {
    private callOnReload: Function;

    constructor() {}

    reload() {
        this.callOnReload();
    }

    setCallBack(func: Function) {
        this.callOnReload = func;
    }
}