import { Component, OnInit, Input } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-register-verify',
  templateUrl: './register-verify.component.html',
  styleUrls: ['./register-verify.component.css']
})
export class RegisterVerifyComponent implements OnInit {
  @Input() displayObject : object;
  contactObject : object = undefined;

  constructor() { }

  ngOnInit() {
    if (this.displayObject["contact"] !== undefined) {
      this.contactObject = JSON.parse(this.displayObject["contact"]);
      delete this.displayObject["contact"];
    }
  }

  submit() : void {
    let sendForm = new FormData();
    sendForm.append('user_name', this.displayObject['真實姓名']);
    sendForm.append('user_mail', this.displayObject['Email']);
    sendForm.append('code', (document.querySelector('#verifyText') as HTMLInputElement).value);
    fetch(AppComponent.API_URL + 'verify_validation_code/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: sendForm
    }).then(res => res.json()).then(res => {
      if (res['status']) {
        alert('驗證成功, 轉跳登入頁面!!');
        window.open('/login', '_self');
      } else {
        alert('驗證碼錯誤，請重新輸入!!');
        (document.querySelector('#verifyText') as HTMLInputElement).value = '';
      }
    })
  }
}
