import { Component, OnInit } from '@angular/core';
import Interpreter from 'js-interpreter';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  private easterClicked = 0;
  private preClick = 0;
  private checker = setInterval(this.checkClick, 1000);
  private interpreter = new Interpreter('', this.initFunc);

  constructor() { }

  ngOnInit() {
  }

  get isDelveloper(): boolean { return this.easterClicked === -1; }

  easterClick(ev: MouseEvent) {
    if (this.easterClicked === -1) {
      alert('你已經是開發人員了!!');
      return;
    }
    ++this.easterClicked;
    if (this.easterClicked === 7) {
      clearInterval(this.checker);
      this.easterClicked = -1;
      alert('你已經成為開發人員!!');
    }
  }

  checkClick() {
    if (this.easterClicked === this.preClick) this.easterClicked = 0;
    this.preClick = this.easterClicked;
  }

  input(ev: KeyboardEvent) {
    if (ev.keyCode === 13) {
      const input = (document.getElementById('interpreter-input') as HTMLInputElement);
      const shell = (document.getElementById('shell') as HTMLDivElement);
      this.interpreter.appendCode(input.value);
      let command = document.createElement('p');
      let result = document.createElement('p');

      try {
        this.interpreter.run();
        result.innerText = this.interpreter.value;
      } catch (ex) {
        result.innerText = ex;
      }
      command.innerText = `>>> ${input.value}`;
      
      shell.appendChild(command);
      shell.appendChild(result);
      
      (document.getElementById('anchor') as HTMLDivElement).scrollIntoView();
      input.value = '';
      return false;
    }
  }

  initFunc(int, scope) {
    var alert_wrapper = function(text) {
      return alert(text);
    };
    int.setProperty(scope, 'alert', int.createNativeFunction(alert_wrapper));
  };
}
