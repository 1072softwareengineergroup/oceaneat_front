import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HeadbarModule } from '../../headbar/headbar.module';
import { ReportComponent } from './report.component';
import { ReportRoutingModule } from './report-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReportRoutingModule,
        HeadbarModule
    ],
    declarations: [
        ReportComponent
    ]
})
export class ReportModule { }