import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { isNumber } from 'util';

@Component({
  selector: 'app-type-card',
  templateUrl: './type-card.component.html',
  styleUrls: ['./type-card.component.css']
})
export class TypeCardComponent implements OnInit, AfterViewInit {
  static readonly DefaultTypeName = '菜單';
  @Input() typeName = this.DefaultTypeName;
  @Input() itemObj: object = null;
  @Output() onTypeDelete = new EventEmitter<string>();
  @Output() onChangeEvent = new EventEmitter<object>();
  @ViewChildren(TypeCardComponent) typeCard: QueryList<TypeCardComponent>;

  typeList: string[] = [];
  items: Map<string, number> = new Map<string, number>();
  editingCells: string[] = [];
  showItem = true;
  
  constructor() { }

  ngOnInit() {
    if (this.itemObj) this.setFromJsonObj(this.itemObj);
  }

  ngAfterViewInit() {
    const r = Math.floor(Math.random() * 90) + 80;
    const g = Math.floor(Math.random() * 90) + 80;
    const b = Math.floor(Math.random() * 90) + 80;
    const element = (document.querySelector('#container-' + this.getEncodeTypeName()) as HTMLDivElement);
    if (element) element.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
  }

  getEncodeTypeName(): string {
    let result = this.typeName.charCodeAt(0);
    for (let i = 1; i < this.typeName.length; ++i) result = 31 * result + this.typeName.charCodeAt(i);
    return result.toString();
  }

  get DefaultTypeName() {
    return TypeCardComponent.DefaultTypeName;
  }

  add() {
    const selected = (document.querySelector('#typeSelect-' + this.typeName) as HTMLSelectElement).value;
    const inputText = (document.querySelector('#inputText-' + this.typeName) as HTMLInputElement).value.trim();

    if (inputText === '') return;

    if (selected === 'type') {
      this.typeList.push(inputText);
    } else {
      this.items.set(inputText, 0);
    }
    this.triggerSave();
  }

  triggerSave() {
    if (this.typeName === this.DefaultTypeName) this.onChangeEvent.emit(this.getJsonObj());
    else this.onChangeEvent.emit();
  }

  deleteItem(iName: string): void {
    if (!confirm(`確認刪除${iName}? (此動作無法復原)`)) return;
    this.items.delete(iName);
    this.triggerSave();
  }
  onTitleRightClick(event: MouseEvent) {
    if (confirm(`確認刪除 ${this.typeName} ？`)) this.onTypeDelete.emit(this.typeName);
    return false;
  }
  deleteType(tName: string): void {
    this.typeList = this.typeList.filter(t => t !== tName);
    this.triggerSave();
  }

  editCell(iName: string): void {
    if (this.editingCells.includes(iName)) return;
    let cell = (document.querySelector(`#${this.typeName}-${iName}`) as HTMLTableDataCellElement);
    this.editingCells.push(iName);
    cell.className = 'col-price-editing';
    cell.contentEditable = 'true';
    if (this.items.get(iName) === 0) cell.innerHTML = '';
    else cell.innerHTML = this.items.get(iName).toString();
    cell.title = '按下Enter完成編輯';
    cell.focus();
  }
  editingCell(event: KeyboardEvent, iName: string) {
    if (event.keyCode === 13) {
      let cell = (document.querySelector(`#${this.typeName}-${iName}`) as HTMLTableDataCellElement);
      let cellVal = parseFloat(cell.innerHTML);
      if (isNaN(cellVal)) {
        alert('請輸入數字!!');
      } else if (cellVal < 0) {
        alert('請輸入大於0的數字!!');
      } else {
        cell.contentEditable = 'false';
        cell.className = 'col-price';
        cell.title = '點擊修改價錢';
        this.editingCells = this.editingCells.filter(c => c !== iName);
        cell.innerHTML = `$${cellVal}`;
        this.items.set(iName, cellVal);
      }
      this.triggerSave();
      return false;
    }
  }

  getJsonObj(): object {
    let obj = {};
    this.items.forEach((val, key) => obj[key] = val);
    this.typeCard.toArray().forEach(card => obj[card.typeName] = card.getJsonObj());
    if (this.typeName === this.DefaultTypeName) console.log(obj);
    return obj;
  }

  setFromJsonObj(jsonObj: object) {
    if (this.typeName === this.DefaultTypeName) {
      this.itemObj = jsonObj;
      this.typeList = [];
      this.items = new Map<string, number>();
    }
    for (const o in jsonObj) {
      if (isNumber(jsonObj[o])) this.items.set(o, jsonObj[o]);
      else this.typeList.push(o);
    }
  }

  getObjByKey(key: string): object {
    return this.itemObj[key];
  }
}
