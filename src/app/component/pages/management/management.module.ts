import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { OtherContactMenuModule } from '../../other-contact-menu/other-contact-menu.module';
import { HeadbarModule } from '../../headbar/headbar.module';
import { ManagementRoutingModule } from './management-routing.module';
import { ManagementComponent } from './management.component';
import { TypeCardComponent } from './type-card/type-card.component';
import { MapModule } from '../map/map.module';

@NgModule({
    imports: [
        CommonModule,
        ManagementRoutingModule,
        HeadbarModule,
        OtherContactMenuModule,
        MapModule
    ],
    declarations: [
        ManagementComponent,
        TypeCardComponent
    ]
})
export class ManagementModule { }