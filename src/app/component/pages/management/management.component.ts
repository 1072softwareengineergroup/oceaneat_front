import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreInfo } from '../../StoreInfo';
import { AppComponent } from 'src/app/app.component';
import { CustomerInfo } from '../../UserInfo';
import { TypeCardComponent } from './type-card/type-card.component';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css', './management.component.mobile.css']
})
export class ManagementComponent implements OnInit {
  @ViewChild(TypeCardComponent) menuCard: TypeCardComponent;
  editable : boolean = true;
  blankUser: boolean = true;
  currentStore: number = 0;
  ownerData: CustomerInfo;
  static storeInfos: StoreInfo[] = [];
  static blankStore = StoreInfo.createBlank();

  constructor() { }

  ngOnInit() {
    fetch(AppComponent.API_URL + `api/Customer/${AppComponent.UserId}/`, {method: 'GET'})
    .then(res => res.json())
    .then(data => {
      this.ownerData = CustomerInfo.createFromJsonObj(data);
      this.getStoreData();
    });
  }

  get stores(): StoreInfo[] {
    return ManagementComponent.storeInfos;
  }
  set stores(list: StoreInfo[]) {
    ManagementComponent.storeInfos = list;
  }

  get storeInfo(): StoreInfo {
    if (ManagementComponent.storeInfos.length === 0) {
      return ManagementComponent.blankStore;
    }
    return ManagementComponent.storeInfos[this.currentStore];
  }

  getStoreData() {
    fetch(AppComponent.API_URL + `api/Restaurant/get_restaurant/?rmid=${AppComponent.UserId}`, { method: 'GET' })
    .then(res => res.json()).then(data => {
      let storeTmp = [];
      const resData: object[] = data['restaurants'];
      for (let i = 0; i < resData.length; ++i) {
        const o = resData[i];
        const resObj = StoreInfo.createFromJsonObj(o, this.ownerData);
        fetch(AppComponent.API_URL + 'api/Restaurant/get_dishes_list/?rid=' + resObj.work_uid, { method: 'GET' })
        .then(res => res.json()).then(data => {
          resObj.menu = this.parseDishList(data);
          storeTmp.push(resObj);
        }).then(() => {
          if (i === resData.length - 1) {
            ManagementComponent.storeInfos = storeTmp;
            this.switchStore(0);
          }
        });
      }
    });
  }
  parseDishList(data: object): object {
    const list: object[] = data['dishes'];
    let jsonObj = {};
    for (const obj of list) {
      const itemList: string[] = obj['item'];
      const price = obj['price'];
      let tmpObj = jsonObj;
      for (let i = 0; i < itemList.length; ++i) {
        if (i === itemList.length - 1) {
          tmpObj[itemList[i]] = price;
        } else {
          if (!tmpObj[itemList[i]]) tmpObj[itemList[i]] = {};
          tmpObj = tmpObj[itemList[i]];
        }
      }
    }
    return jsonObj;
  }

  uploadImg() {
    const image = (document.getElementById('storeImgUpload') as HTMLInputElement).files[0];

    let formData = new FormData();
    formData.append('rid', this.storeInfo.work_uid);
    formData.append('image', image);

    fetch(AppComponent.API_URL + 'api/Restaurant/update_picture/', {
      method: 'POST',
      body: formData
    }).then(res => res.json()).then(data => {
      if (!data['status']) console.log(data);
      window.open('/management', '_self');
    });
  }

  addStore() : void {
    const storeName = (<HTMLInputElement>document.querySelector('#storeInput')).value.trim();
    if (!storeName) return;
    const newStore = {
      address: '',
      lacation: '',
      mail_address: this.ownerData.mailAddress,
      phone_number: this.ownerData.phoneNumber,
      restaurant_name: storeName,
      restaurant_owner: this.ownerData.customerId
    };
    fetch(AppComponent.API_URL + 'api/Restaurant/create_restaurant/', {
      credentials: 'include',
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(newStore),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => res.json()).then(data => {
      console.log(data);
      if (data['status']) {
        this.getStoreData();
      } else {
        alert('發生錯誤!!');
        console.log(data);
      }
    });
  }

  deleteRestaurant() {
    if (!confirm(`確認刪除 "${this.storeInfo.storeName}" ？(此動作無法復原)`)) return;
    fetch(`${AppComponent.API_URL}api/Restaurant/${this.storeInfo.work_uid}/`, {
      method: 'DELETE'
    }).then(res => {
      this.getStoreData();
    });
  }

  nameEdit = false;
  phoneEdit = false;
  addressEdit = false;
  principalEdit = false;
  emailEdit = false;
  
  toggleNameEdit() {
    if (this.nameEdit) {
      const inputVal = (document.getElementById('nameInput') as HTMLInputElement).value;
      this.storeInfo.storeName = inputVal;
      this.saveData();
    }
    this.nameEdit  = !this.nameEdit;
  }
  togglePhoneEdit() {
    if (this.phoneEdit) {
      const inputVal = (document.getElementById('phoneInput') as HTMLInputElement).value;
      this.storeInfo.phone = inputVal;
      this.saveData();
    }
    this.phoneEdit = !this.phoneEdit;
  }
  toggleAddressEdit() {
    const container = (document.getElementById('container') as HTMLDivElement)
    if (this.addressEdit) {
      const inputVal = (document.getElementById('addressInput') as HTMLInputElement).value;
      this.storeInfo.location = inputVal;
      container.style.height = 'auto';
      container.style.overflowY = 'auto';
      this.saveData();
    } else {
      container.style.height = '0';
      container.style.overflowY = 'hidden';
    }
    this.addressEdit = !this.addressEdit;
  }
  togglePrincipalEdit() {
    if (this.principalEdit) {
      const inputVal = (document.getElementById('principalInput') as HTMLInputElement).value;
      this.storeInfo.principal = inputVal;
      this.saveData();
    }
    this.principalEdit = !this.principalEdit;
  }
  toggleEmailEdit() {
    if (this.emailEdit) {
      const inputVal = (document.getElementById('emailInput') as HTMLInputElement).value;
      this.storeInfo.email = inputVal;
      this.saveData();
    }
    this.emailEdit = !this.emailEdit;
  }

  setAddress(address: object) {
    this.storeInfo.location = address['address'];
    this.storeInfo.position = address['position'];
    console.log(this.storeInfo.position);
    this.toggleAddressEdit();
    this.saveData();
  }

  isSelected(id: any): boolean {
    return this.stores[this.currentStore].work_uid === id;
  }

  getCurrentName(): string {
    const store = this.stores[this.currentStore];
    if (store) return store.storeName;
    return '';
  }

  switchStore(storeId: any) {
    this.blankUser = false;
    if (this.stores.length === 0) {
      this.blankUser = true;
      return;
    }

    for(let index = 0; index < this.stores.length; ++index) {
      if (this.stores[index].work_uid === storeId) {
        this.currentStore = index;
        break;
      }
    }
    if (this.storeInfo.menu) setTimeout(() => {this.menuCard.setFromJsonObj(this.storeInfo.menu)}, 150);
  }

  saveData() {
    let jsonObj = this.storeInfo.getFormatJsonObj();
    jsonObj['restaurant_owner'] = AppComponent.UserId;
    fetch(AppComponent.API_URL + 'api/Restaurant/update_restaurant/', {
      credentials: 'include',
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(jsonObj),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => res.json()).then(console.log);
  }
  saveMenu(menu: object) {
    this.storeInfo.menu = menu;
    const bodyJson = {
      restaurant_id: this.storeInfo.work_uid,
      name: this.storeInfo.storeName,
      root: menu
    };
    fetch(AppComponent.API_URL + 'api/Restaurant/update_restaurant_dishes/', {
      credentials: 'include',
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(bodyJson),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => {
      console.log('Update SearchIndex');
      fetch(AppComponent.API_URL + 'api/Restaurant/update_search_index/')
      .then(res => {
        console.log(res);
        console.log('Reloading');
        fetch(AppComponent.API_URL + 'api/SearchIndex/reload/').then(console.log);
      });
    });
  }
}
