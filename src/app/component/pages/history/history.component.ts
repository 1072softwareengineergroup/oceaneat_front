import { Component, OnInit } from '@angular/core';
import { OrderInfo, OrderItem } from '../../OrderInfo';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { CustomerInfo } from '../../UserInfo';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  orderInfos : OrderInfo[] = [];
  currentOrderId : string = undefined;
  selectedOrder: OrderInfo = undefined;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.currentOrderId = params["id"];
    });

    const api_url = AppComponent.isDelivery 
      ? AppComponent.API_URL + 'api/Order/query_order_by_delivery/?delivery_id='
      : AppComponent.API_URL + 'api/Order/query_order/?customer_id=';
    fetch(api_url + AppComponent.UserId, {
      method: 'GET'
    }).then(res => res.json()).then(data => {
      this.orderInfos = [];
      let index = 0;
      for (let obj of data['orders']) {
        let order = OrderInfo.createFromObj(obj);

        if (order.deliveryStaff) {
          fetch(AppComponent.API_URL + `api/Customer/${order.deliveryStaff}/`, {method: 'GET'})
          .then(res => res.json()).then(data => {
            const deliveryObj = CustomerInfo.createFromJsonObj(data);
            order.deliveryStaffObj = deliveryObj;
            order.deliveryStaff = `${deliveryObj.userName} ${deliveryObj.nickName ? `(${deliveryObj.nickName})` : ''}`;
          });
        } else {
          if (order.isError()) order.deliveryStaff = '-';
          else order.deliveryStaff = '等待中...';
        }
        this.orderInfos[index++] = order;
        this.setIfIsCurrentOrder(order);
      }
    });
  }

  toggleShowInfo(order : OrderInfo) : void {
    order['showInfo'] = !order['showInfo']
  }

  setIfIsCurrentOrder(order: OrderInfo) {
    if (this.currentOrderId && this.currentOrderId === order.orderId) this.selectedOrder = order;
  }

  getReverseList(): OrderInfo[] {
    let tmpList = [];
    for (let i = this.orderInfos.length - 1; i >= 0; --i) tmpList.push(this.orderInfos[i]);
    return tmpList;
  }
}
