import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { OrderInfo } from 'src/app/component/OrderInfo';
import { AppComponent } from 'src/app/app.component';
import { RatingStarsComponent } from 'src/app/component/rating-stars/rating-stars.component';

@Component({
  selector: 'app-order-info-display',
  templateUrl: './order-info-display.component.html',
  styleUrls: ['./order-info-display.component.css']
})
export class OrderInfoDisplayComponent implements OnInit {
  @Input() order : OrderInfo
  @Input() inTable : boolean
  @ViewChild(RatingStarsComponent) ratingStars: RatingStarsComponent;

  constructor() { }

  ngOnInit() {
    let timer = setInterval(() => {
      let infoInTable = (document.getElementById('info-in-table') as HTMLSpanElement);
      if (this.order && infoInTable) {
        this.order.toStringPromise().then(res => {
          infoInTable.innerText = res;
        });
        clearInterval(timer);
      }
    }, 100);
  }

  get isDelivery(): boolean {
    return AppComponent.isDelivery
  }

  deleteOrder() {
    if (!confirm('確認刪除訂單？ (此操作無法復原)')) return;

    fetch(AppComponent.API_URL + 'api/Order/delete_order/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({order_id: this.order.orderId}),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => {
      if (res.status !== 200) {
        console.log(res);
        console.log(res.status);
      } else {
        this.order.error();
        window.open(window.location.href, '_self');
      }
    }).catch(console.error);
  }

  finishOrder() {
    if (this.ratingStars.getRating() === 0 && !confirm('尚未進行評價，確認完成訂單？ (此操作無法復原)')) return;
    else if (!confirm('確認完成訂單？ (此操作無法復原)')) return;
    this.ratingStars.submitRating();

    fetch(AppComponent.API_URL + 'api/Order/finish_order/', {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        order_id: this.order.orderId,
        acceptor: (AppComponent.isDelivery) ? 'delivery' : 'user'
      }),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => {
      if (res.status !== 200) {
        console.log(res);
        console.log(res.status);
      } else {
        this.order.pending();
        window.open(window.location.href, '_self');
      }
    }).catch(console.error);
  }

  isUser(): boolean {
    return !AppComponent.isDelivery
  }
}
