import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderInfoDisplayComponent } from './order-info-display.component';

describe('OrderInfoDisplayComponent', () => {
  let component: OrderInfoDisplayComponent;
  let fixture: ComponentFixture<OrderInfoDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderInfoDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderInfoDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
