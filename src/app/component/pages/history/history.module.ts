import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HeadbarModule } from '../../headbar/headbar.module';
import { HistoryRoutingModule } from './history-routing.module';
import { HistoryComponent } from './history.component';
import { OrderInfoDisplayComponent } from './order-info-display/order-info-display.component';
import { RatingStarsComponent } from '../../rating-stars/rating-stars.component';
import { OrderDisplayModule } from '../../order-display/order-display.module';

@NgModule({
    imports: [
        CommonModule,
        HistoryRoutingModule,
        HeadbarModule,
        OrderDisplayModule
    ],
    declarations: [
        HistoryComponent,
        OrderInfoDisplayComponent,
        RatingStarsComponent
    ]
})
export class HistoryModule { }