import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSubNormalComponent } from './register-sub-normal.component';

describe('RegisterSubNormalComponent', () => {
  let component: RegisterSubNormalComponent;
  let fixture: ComponentFixture<RegisterSubNormalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSubNormalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSubNormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
