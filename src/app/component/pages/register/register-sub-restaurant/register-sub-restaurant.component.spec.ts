import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSubRestaurantComponent } from './register-sub-restaurant.component';

describe('RegisterSubRestaurantComponent', () => {
  let component: RegisterSubRestaurantComponent;
  let fixture: ComponentFixture<RegisterSubRestaurantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSubRestaurantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSubRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
