import { trigger, state, style, transition, animate } from '@angular/animations';

export const ShowHideAnimation = trigger('showHide', [
    state('true', style({
        opacity: 1,
        display: 'block'
    })),
    state('false', style({
        opacity: 0,
        display: 'none'
    })),
    transition('true => false', [
    ]),
    transition('false => true', [
        style({display: 'block'}),
        animate('750ms')
    ])
])