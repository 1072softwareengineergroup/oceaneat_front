import { OnInit, Input } from '@angular/core';

export abstract class RegisterSubBaseComponent implements OnInit {
  @Input() show : boolean;

  constructor() { }

  ngOnInit() {
  }

  abstract getData() : any;
}
