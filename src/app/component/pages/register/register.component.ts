import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { RegisterSubBaseComponent } from './register-sub-base/register-sub-base.component';
import { RegisterSubNormalComponent } from './register-sub-normal/register-sub-normal.component';
import { RegisterSubDeliveryComponent } from './register-sub-delivery/register-sub-delivery.component';
import { RegisterSubRestaurantComponent } from './register-sub-restaurant/register-sub-restaurant.component';
import { AppComponent } from 'src/app/app.component';
import * as sha1 from 'js-sha1';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit, AfterViewInit {
  @ViewChild(RegisterSubNormalComponent) normalTable : RegisterSubNormalComponent
  @ViewChild(RegisterSubDeliveryComponent) deliveryTable : RegisterSubDeliveryComponent
  @ViewChild(RegisterSubRestaurantComponent) restaurantTable : RegisterSubRestaurantComponent
  tables : RegisterSubBaseComponent[] = []
  displayObject : Object = new Object()
  
  displayDecoder = {
    "phone_number": "電話",
    "mail_address": "Email",
    "user_name": "真實姓名",
    "gender": "性別",
    "nick_name": "暱稱",
    "address": "地址",
    "other_contact": "contact"
  };

  selected : number = 0
  submited : boolean = false

  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit() { 
    this.tables.push(this.normalTable)
    this.tables.push(this.deliveryTable)
    this.tables.push(this.restaurantTable)
  }

  submit() : void {
    let data = this.tables[this.selected].getData()
    if (data === null) return

    for (let tag in data) {
      if (data[tag] === '') continue;
      if (this.displayDecoder[tag] === undefined) continue;
      this.displayObject[this.displayDecoder[tag]] = data[tag];
    }
    this.submited = true


    let api_loc = AppComponent.API_URL + 'api/'
    switch (this.selected) {
      case 0:
        api_loc += 'Customer/create_customer'
        break;
      case 1:
        api_loc += 'Delivery/create_delivery'
        break;
      case 2:
        api_loc += 'RestaurantManager/create_restaurant_manager'
        break;
    }
    api_loc += '/'

    data['psw'] = sha1(data['psw']);
    fetch(api_loc, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(res => {
      if (res['status']) {
        let verifyBody = new FormData();
        verifyBody.append('user_mail', data['mail_address']);
        verifyBody.append('user_name', data['user_name']);
        fetch(AppComponent.API_URL + 'mailcheck/', {
          method: 'POST',
          mode: 'cors',
          credentials: 'include',
          body: verifyBody
        }).then(res => res.json()).then(console.log);
      } else {
        console.log(res);
        alert('錯誤的操作!!');
        this.submited = false;
      }
    });
  }
}
