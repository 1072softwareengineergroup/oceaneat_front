import { Component, OnInit, ViewChild } from '@angular/core';
import { RegisterSubBaseComponent } from '../register-sub-base/register-sub-base.component';
import { ShowHideAnimation } from '../register-sub-base/show-hide-animation';
import { OtherContactMenuComponent } from '../../../other-contact-menu/other-contact-menu.component';

@Component({
  selector: 'app-register-sub-delivery',
  templateUrl: './register-sub-delivery.component.html',
  styleUrls: ['../register-sub-base/register-sub-base.component.css'],
  animations: [ ShowHideAnimation ]
})
export class RegisterSubDeliveryComponent extends RegisterSubBaseComponent implements OnInit {
  @ViewChild(OtherContactMenuComponent) contactMenu : OtherContactMenuComponent;

  readonly ID : number = 1;

  constructor() { super(); }

  ngOnInit() {
  }

  getData() : any {
    let phone = (<HTMLInputElement>document.querySelectorAll('input[name="phone"]')[this.ID]).value.trim();
    let email = (<HTMLInputElement>document.querySelectorAll('input[name="email"]')[this.ID]).value.trim();
    let name = (<HTMLInputElement>document.querySelectorAll('input[name="name"]')[this.ID]).value.trim();
    let password = (<HTMLInputElement>document.querySelectorAll('input[name="pass"]')[this.ID]).value.trim();
    let passowrdVerify = (<HTMLInputElement>document.querySelectorAll('input[name="passCheck"]')[this.ID]).value.trim();
    let gender = (<HTMLInputElement>document.querySelectorAll('input[name="gender"]')[this.ID]).value.trim();
    let nick = (<HTMLInputElement>document.querySelectorAll('input[name="nick"]')[this.ID]).value.trim();
    let location = (<HTMLInputElement>document.querySelectorAll('input[name="location"]')[this.ID]).value.trim();

    if (phone.length === 0 || email.length === 0 || password.length === 0 || passowrdVerify.length === 0 || !this.contactMenu.hasValue()) {
      alert('資料不完整!!');
      return null;
    }
    if (password !== passowrdVerify) {
      (<HTMLInputElement>document.querySelectorAll('input[name="pass"]')[0]).value = '';
      (<HTMLInputElement>document.querySelectorAll('input[name="passCheck"]')[this.ID]).value = '';
      alert('密碼不正確!!');
      return null;
    }

    return {
      'phone_number': phone,
      'mail_address': email + '@mail.ntou.edu.tw',
      'user_name': name,
      'psw': password,
      'gender': gender,
      'nick_name': nick,
      'address': location,
      'other_contact': this.contactMenu.getContact()
    };
  }
}
