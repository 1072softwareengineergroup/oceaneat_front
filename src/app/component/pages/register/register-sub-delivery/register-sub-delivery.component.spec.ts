import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSubDeliveryComponent } from './register-sub-delivery.component';

describe('RegisterSubDeliveryComponent', () => {
  let component: RegisterSubDeliveryComponent;
  let fixture: ComponentFixture<RegisterSubDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSubDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSubDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
