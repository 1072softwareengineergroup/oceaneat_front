import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterSubNormalComponent } from './register-sub-normal/register-sub-normal.component';
import { RegisterSubDeliveryComponent } from './register-sub-delivery/register-sub-delivery.component';
import { RegisterSubRestaurantComponent } from './register-sub-restaurant/register-sub-restaurant.component';
import { HeadbarModule } from '../../headbar/headbar.module';
import { RegisterVerifyComponent } from '../register-verify/register-verify.component';
import { OtherContactMenuModule } from '../../other-contact-menu/other-contact-menu.module';

@NgModule({
    imports: [
        CommonModule,
        RegisterRoutingModule,
        HeadbarModule,
        OtherContactMenuModule
    ],
    declarations: [
        RegisterComponent,
        RegisterSubNormalComponent,
        RegisterSubDeliveryComponent,
        RegisterSubRestaurantComponent,
        RegisterVerifyComponent
    ]
})
export class RegisterModule { }