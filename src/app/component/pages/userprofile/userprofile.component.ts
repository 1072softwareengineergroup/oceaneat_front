import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CustomerInfo } from '../../UserInfo';
import { OtherContactMenuComponent } from '../../other-contact-menu/other-contact-menu.component';
import { MapComponent } from '../map/map.component';

import * as sha1 from 'js-sha1';
import * as p5 from 'p5';

declare function markdownNow(): any;

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit, AfterViewInit {
  private yoff = 0.0;
  isDescriptionEditing : boolean = false;
  contactEditable : boolean = false;
  editable : boolean = true;
  userInfo: CustomerInfo = null;
  isDeliveryCBox: HTMLInputElement;
  @ViewChild(OtherContactMenuComponent) otherContact: OtherContactMenuComponent;
  @ViewChild(MapComponent) map: MapComponent;

  constructor() { }

  ngOnInit() {
    (<HTMLInputElement>document.querySelector('#isDelivery')).disabled = !this.editable

    fetch(AppComponent.API_URL + `api/Customer/${AppComponent.UserId}/`, {
      method: 'GET'
    }).then(res => res.json()).then(data => {
      this.userInfo = CustomerInfo.createFromJsonObj(data);
      (document.getElementById('markdownEditor') as HTMLTextAreaElement).value = this.userInfo.discribe;
      markdownNow();
      this.isDeliveryCBox = (document.getElementById('isDelivery') as HTMLInputElement);
      this.isDeliveryCBox.checked = this.userInfo.deliveryMode;

      this.map.addressToPosition(this.userInfo.address).then(position => {
        if (position) {
          localStorage['curLocation'] = JSON.stringify({address: this.userInfo.address, position});
        }
      });
    });

    (document.getElementById('userImg') as HTMLImageElement).src = AppComponent.API_URL + "api/Customer/get_picture/?customer_id=" + AppComponent.UserId;
    (document.getElementById('markdownEditor') as HTMLTextAreaElement).addEventListener('keyup', () => {
      this.userInfo.discribe = (document.getElementById('markdownEditor') as HTMLTextAreaElement).value;
      this.saveData();
    });
  }

  ngAfterViewInit() {
    this.startP5();
  }

  uploadImg() {
    const image = (document.getElementById('imgUpload') as HTMLInputElement).files[0];

    let formData = new FormData();
    formData.append('customer_id', AppComponent.UserId);
    formData.append('image', image);

    fetch(AppComponent.API_URL + 'api/Customer/update_picture/', {
      method: 'POST',
      body: formData
    }).then(res => res.json()).then(data => {
      if (!data['status']) console.log(data);
      window.open('/userprofile', '_self');
    });
  }

  toggleDescriptionEdit() : void {
    if (this.isDescriptionEditing) {
      this.userInfo.discribe = (document.getElementById('markdownEditor') as HTMLTextAreaElement).value;
      this.saveData();
    }
    this.isDescriptionEditing = !this.isDescriptionEditing;
  }

  toggleContactEdit() : void {
    if (this.contactEditable) {
      this.userInfo.otherContact = this.otherContact.contactDict;
      this.saveData();
    }
    this.contactEditable = !this.contactEditable;
  }

  getName(): string {
    if (this.userInfo === null) return '';
    const nick = this.userInfo.nickName;
    const name = this.userInfo.userName;
    if (nick === '' || nick === null) {
      return '';
    }
    return ' / ' + name;
  }
  getNick(): string {
    if (this.userInfo === null) return '';
    const nick = this.userInfo.nickName;
    const name = this.userInfo.userName;
    if (nick === '' || nick === null) {
      return name;
    }
    return nick;
  }
  getRank(): string {
    if (!this.userInfo) return '';
    return `${this.userInfo.customerRank} (${this.userInfo.customerRankTimes}次)`;
  }

  get notRestaurant(): boolean { return !AppComponent.isRestaurant }

  nameEdit = false;
  pswEdit = false;
  genderEdit = false;
  emailEdit = false;
  phoneEdit = false;
  addressEdit = false;

  toggleEditName() {
    if (this.nameEdit) {
      const name = (document.getElementById('nameInput') as HTMLInputElement).value;
      const nick = (document.getElementById('nickInput') as HTMLInputElement).value;
      this.userInfo.userName = name;
      this.userInfo.nickName = nick;
      this.saveData();
    }
    this.nameEdit = !this.nameEdit;
  }
  toggleEditPassword() {
    if (this.pswEdit) {
      const oldPass = (document.getElementById('oldPassInput') as HTMLInputElement);
      const newPass = (document.getElementById('newPassInput') as HTMLInputElement);
      const newPassConfirm = (document.getElementById('newPassInputConfirm') as HTMLInputElement);

      if (oldPass.value === '' && newPass.value === '' && newPassConfirm.value === '') {
        this.pswEdit = !this.pswEdit;
        return;
      }
      if (sha1(oldPass.value) !== this.userInfo.password) {
        alert('舊密碼錯誤！！');
        oldPass.value = '';
        return;
      }
      if (newPass.value !== newPassConfirm.value) {
        alert('驗證密碼錯誤！！');
        newPassConfirm.value = '';
        return;
      }
      this.userInfo.password = sha1(newPass.value);
      this.saveData();
    }
    this.pswEdit = !this.pswEdit;
  }
  toggleEditGender() {
    if (this.genderEdit) {
      const gender = (document.getElementById('genderInput') as HTMLInputElement).value;
      this.userInfo.gender = gender;
      this.saveData();
    }
    this.genderEdit = !this.genderEdit;
  }
  toggleEditEmail() {
    if (this.emailEdit) {
      const mail = (document.getElementById('mailInput') as HTMLInputElement).value;
      this.userInfo.mailAddress = mail;
      this.saveData();
    }
    this.emailEdit = !this.emailEdit;
  }
  toggleEditPhone() {
    if (this.phoneEdit) {
      const phone = (document.getElementById('phoneInput') as HTMLInputElement).value;
      this.userInfo.phoneNumber = phone;
      this.saveData();
    }
    this.phoneEdit = !this.phoneEdit;
  }
  toggleEditAddress() {
    this.addressEdit = !this.addressEdit;
    const container = (document.getElementById('container') as HTMLDivElement);
    if (this.addressEdit) {
      container.style.height = '0';
      container.style.overflowY = 'hidden';
    } else {
      container.style.height = 'auto';
      container.style.overflowY = 'auto';
    }
  }
  setAddress(address: object) {
    this.userInfo.address = address['address'];
    localStorage['curLocation'] = JSON.stringify(address);
    this.toggleEditAddress();
    this.saveData();
  }
  
  isDeliveryValid(): boolean {
    const obj = this.otherContact.getContact();
    if (JSON.stringify(obj) === '"{}"') {
      if (this.userInfo !== null) {
        this.userInfo.deliveryMode = false;
        this.isDeliveryCBox.checked = this.userInfo.deliveryMode;
      }
      return false;
    }
    return true;
  }

  saveData() {
    this.userInfo.otherContact = this.otherContact.contactDict;
    const saveData = this.userInfo.toFormatJson();
    fetch(AppComponent.API_URL + 'api/Customer/update_customer/', {
      mode: 'cors',
      credentials: 'include',
      method: 'POST',
      body: JSON.stringify(saveData),
      headers: {
        'content-type': 'application/json'
      }
    }).then(res => res.json()).then(data => {
      if (!data['status']) console.log(data);
    });
  }

  startP5() {
    let sketch = (p : any) => { this.makeCanvas(p, p.windowWidth > 760 ? p.windowWidth / 2 : p.windowWidth, 170, 0) };
    new p5(sketch, document.getElementById('wave'));
  }

  makeCanvas(p : any, w : number, h : number, bias : number) : void {
    p.setup = () => {
      p.createCanvas(w - .5, h);
    };
  
    p.draw = () => {
      let width = p.windowWidth > 760 ? p.windowWidth / 2 : p.windowWidth;

      p.clear();
      p.fill(0,119,200);
      p.noStroke();
      p.beginShape();
  
      let xoff = 0;
  
      for (let x = 0; x <= width - .5 + 5; x += 10) {
        let y = p.map(p.noise(xoff, this.yoff), 0, 1, 50, 200);
        p.vertex(x, y);
        xoff += 0.05;
      }
      this.yoff += 0.01;
      p.vertex(width - .5 + 5, h);
      p.vertex(0, h);
      p.endShape(p.CLOSE);
    }

    p.windowResized = () => {
      p.clear();
      if (window.screen.width > 760) p.resizeCanvas(window.screen.width / 2 - .5 - bias, h);
      else p.resizeCanvas(window.screen.width - .5 - bias, h);
    }
  }
}
