import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { UserprofileRoutingModule } from './userprofile-routing.module';
import { UserprofileComponent } from './userprofile.component';
import { OtherContactMenuModule } from '../../other-contact-menu/other-contact-menu.module';
import { HeadbarModule } from '../../headbar/headbar.module';
import { MapModule } from '../map/map.module';

@NgModule({
    imports: [
        CommonModule,
        UserprofileRoutingModule,
        HeadbarModule,
        OtherContactMenuModule,
        MapModule
    ],
    declarations: [
        UserprofileComponent
    ]
})
export class UserprofileModule { }