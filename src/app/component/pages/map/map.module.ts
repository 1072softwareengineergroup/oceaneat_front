import { NgModule } from "@angular/core";
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map.component';
import { MapRoutingModule } from './map-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MapRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDKNCospDzFb7ZTxzvz3vlTojc7Wz6rLXQ',
      libraries: ["places"]
    })
  ],
  declarations: [
    MapComponent
  ],
  exports: [
    MapComponent
  ]
})
export class MapModule { }