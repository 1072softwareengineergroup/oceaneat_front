/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { MapsAPILoader, AgmMap, AgmMarker } from '@agm/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private autocomplete: google.maps.places.Autocomplete;
  private placeQuery: google.maps.places.PlacesService;
  private geoCoder: google.maps.Geocoder;
  @Output() submitEvent = new EventEmitter<object>();
  @ViewChild(AgmMap) map: AgmMap;
  @ViewChild(AgmMarker) agmMarker: AgmMarker;

  lat = 25.15044;
  lng = 121.7755731;
  marker = { lat: 25.15044, lng: 121.7755731 };
  zoom = 12;

  constructor(private mapApiLoader: MapsAPILoader) { }

  ngOnInit() {
    this.mapApiLoader.load().then(() => {
      this.autocomplete = new google.maps.places.Autocomplete(document.getElementById('addressInput') as HTMLInputElement); 
      this.geoCoder = new google.maps.Geocoder;
    });
  }
  mapReady($event: google.maps.Map) {
    this.placeQuery = new google.maps.places.PlacesService($event);
    this.setCurrentLocation();
  }

  search(): void {
    let input = (document.getElementById('addressInput') as HTMLInputElement).value.trim();
    if (input === '') return;
    const request = { query: input, fields: ['geometry'] };

    let place = this.autocomplete.getPlace();
    if (place === undefined) {  
      this.placeQuery.findPlaceFromQuery(request, (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
          place = results[0];
          this.setPosition(place.geometry.location.lat(), place.geometry.location.lng());
          (document.getElementById('addressInput') as HTMLInputElement).focus();
        } else { alert('查無地址!!請直接在地圖上點選正確位置或重新輸入!!'); }
      });
    } else {
      this.setPosition(place.geometry.location.lat(), place.geometry.location.lng());
    }
  }

  addressToPosition(address: string): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      const request = { query: address, fields: ['geometry'] };
      if (address) {
        let time = 0;
        const id = setInterval(() => {
          ++time;
          if (this.placeQuery) {
            this.placeQuery.findPlaceFromQuery(request, (results, status) => {
              if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
                const place = results[0];
                resolve({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});
                clearInterval(id);
              }
              else { resolve(null); clearInterval(id); }
            });
          }
          if (time === 10) { resolve(null); clearInterval(id); };
        }, 200);
      }
    });
  }

  setPosition(lat: number, lng: number) {
    this.marker = { lat, lng };
    this.getAddress();
  }

  setCurrentLocation() {
    const loc = AppComponent.CurLocation;
    if (loc) {
      this.setPosition(loc['position']['lat'], loc['position']['lng']);
      (document.getElementById('addressInput') as HTMLInputElement).value = loc['address'];
    } else {
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.setPosition(position.coords.latitude, position.coords.longitude);
          this.lat = this.marker.lat;
          this.lng = this.marker.lng;
        });
      }
    }
  }

  getAddress(): void {
    this.geoCoder.geocode({'location': this.marker}, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK && results[0]) {
        (document.getElementById('addressInput') as HTMLInputElement).value = results[0].formatted_address;
      } else { alert('無法自動抓取地址，請手動輸入!!'); }
    });
  }

  submit(): void {
    this.submitEvent.emit({
      address: (document.querySelector('#addressInput') as HTMLInputElement).value,
      position: this.marker
    });
  }
}
