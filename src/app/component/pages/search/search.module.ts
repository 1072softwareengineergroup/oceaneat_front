import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { FloatCartModule } from '../../float-cart/float-cart.modules';
import { HeadbarModule } from '../../headbar/headbar.module';
import { StoreCardComponent } from './store-card/store-card.component';
import { MapModule } from '../map/map.module';

@NgModule({
    imports: [
        CommonModule,
        SearchRoutingModule,
        FloatCartModule,
        HeadbarModule,
        MapModule
    ],
    declarations: [
        SearchComponent,
        StoreCardComponent
    ]
})
export class SearchModule { }