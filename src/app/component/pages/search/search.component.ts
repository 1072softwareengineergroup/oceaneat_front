import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StoreInfo } from '../../StoreInfo';
import { AppComponent } from 'src/app/app.component';
import { StoreCardComponent } from './store-card/store-card.component';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchWord : string;
  isChoosePosition = false;
  storeInfos : StoreInfo[] = []
  @ViewChildren(StoreCardComponent) cards: QueryList<StoreCardComponent>;
  @ViewChild(MapComponent) map: MapComponent;

  constructor(private route : ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.searchWord = param['s']
    })

    if (this.searchWord !== undefined) {
      fetch(AppComponent.API_URL + 'api/SearchIndex/query/?query=' + this.searchWord + '&query_type=menu', {
        method:'GET'
      }).then(res => res.json()).then(res => {
        this.processData(res);
      });
    }

    const pos = AppComponent.CurLocation;
    if (pos) {
      (document.querySelector('#locationInput') as HTMLInputElement).value = pos['address'];
      (document.querySelector('#locationInput') as HTMLInputElement).title = pos['address'];
    } else {
      AppComponent.User.then(res => {
        if (res) {
          (document.getElementById('locationInput') as HTMLInputElement).value = res.address;
          (document.querySelector('#locationInput') as HTMLInputElement).title = res.address;
          this.map.addressToPosition(res.address).then(position => {
            if (position) localStorage['curLocation'] = JSON.stringify({address: res.address, position});
          });
        }
      });
    }
  }

  keyupEvent(ev: KeyboardEvent) {
    if (ev.keyCode === 13) this.search();
  }

  search() : void {
    if ((<HTMLInputElement>document.querySelector('#searchInput')).value === '') {
      window.open('/search', '_self')
      return
    }
    window.open('/search?s=' + (<HTMLInputElement>document.querySelector('#searchInput')).value, '_self')
  }

  searchNearby() {
    if (!AppComponent.CurLocation) {
      alert('請輸入地址！！');
      return;
    }
    const pos = AppComponent.CurLocation['position'];
    fetch(AppComponent.API_URL + `api/Restaurant/get_nearby_restaurant/?x=${pos['lat']}&y=${pos['lng']}`, { method: 'GET' })
    .then(res => res.json()).then(data => {
      this.storeInfos = [];
      let restaurants: object[] = data['restaurant'];
      for (const i in restaurants) {
        const id = restaurants[i];
        fetch(AppComponent.API_URL + `api/Restaurant/${id}/`, {method: 'GET'})
        .then(res => res.json()).then(data => {
          this.storeInfos[i] = StoreInfo.createFromJsonObjNoOwner(data);
          fetch(AppComponent.API_URL + 'api/Restaurant/get_dishes_list/?rid=' + this.storeInfos[i].work_uid, { method: 'GET' })
          .then(res => res.json()).then(data => {
            this.storeInfos[i].menu = this.parseDishList(data);
          });
        });
      }
    });
  }

  processData(data : Array<object>) : void {
    console.log(data)
    for (let o of data) {
      const pos = o['location'] ? o['location'].split(', ') : [0, 0];
      let s = new StoreInfo(
        o['restaurant_name'], o['phone'], o['address'], '', undefined,
        AppComponent.API_URL + 'api/Restaurant/get_picture/?rid=' + o['restaurant_id'], null,
        {lat: pos[0], lng: pos[1]}
        );
      s.menu = o['menu'];
      this.storeInfos.push(s);
    }
  }

  parseDishList(data: object): object {
    const list: object[] = data['dishes'];
    let jsonObj = {};
    for (const obj of list) {
      const itemList: string[] = obj['item'];
      const price = obj['price'];
      let tmpObj = jsonObj;
      for (let i = 0; i < itemList.length; ++i) {
        if (i === itemList.length - 1) {
          tmpObj[itemList[i]] = price;
        } else {
          if (!tmpObj[itemList[i]]) tmpObj[itemList[i]] = {};
          tmpObj = tmpObj[itemList[i]];
        }
      }
    }
    return jsonObj;
  }

  hideEveryone(ev) {
    this.cards.toArray().forEach(c => c.showInfo = false);
  }

  chooseAddress() {
    this.isChoosePosition = true;
  }

  finishChoosing(ev: object) {
    this.isChoosePosition = false;
    localStorage['curLocation'] = JSON.stringify(ev);
    (document.querySelector('#locationInput') as HTMLInputElement).value = ev['address'];
    (document.querySelector('#locationInput') as HTMLInputElement).title = ev['address'];
  }
}
