import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StoreInfo } from 'src/app/component/StoreInfo';
import { MenuItem } from 'src/app/component/MenuInfo';
import { OrderItem } from 'src/app/component/OrderInfo';

@Component({
  selector: 'app-store-card',
  templateUrl: './store-card.component.html',
  styleUrls: ['./store-card.component.css', './store-card.component.mobile.css']
})
export class StoreCardComponent implements OnInit {
  @Input() store : StoreInfo
  @Input() index : number
  @Input() searchWord : string
  @Output() preShow = new EventEmitter();
  showInfo : boolean = false
  aUrl : string
  curOrders : OrderItem[]
  menuInfo: object[] = []
  displayList: object[] = []
  stack: string[] = []

  constructor() { }

  ngOnInit() {
    this.curOrders = [];
  }

  toggleShow() : void {
    if (!this.showInfo) this.preShow.emit();
    this.showInfo = !this.showInfo;
    this.aUrl = this.getUrl();
    this.stack = [];
    this.setMenuDisplay();
  }

  setMenuDisplay(select: string = null, value: string = null): void { 
    if (value !== null && !isNaN(parseFloat(value))) return;
    this.displayList = [];
    let curMenu = this.store.menu;

    if (select === null) {
      curMenu = this.store.menu;
    } else if (select === '..') {
      this.stack.pop();
      for (let path of this.stack)
        curMenu = this.store.menu[path];
    } else {
      this.stack.push(select);
      for (let path of this.stack)
        curMenu = curMenu[path];
    }
    if (this.stack.length > 0)
      this.displayList.push({'..': ''});

    for (let name in curMenu) {
      let price = parseFloat(curMenu[name]);
      let tmp = {};
      if (isNaN(price)) tmp[name] = '';
      else tmp[name] = price;
      this.displayList.push(tmp);
    }
  }

  getSelectedStr(): string {
    let s = '';
    for (let op of this.stack) s += op + ' ';
    return s;
  }

  getUrl() : string {
    let curIndex = this.showInfo ? this.index : -this.index

    if (this.searchWord !== undefined) return `/search?s=${this.searchWord}#${curIndex}`
    return `/search#${curIndex}`
  }

  addToOrder() : void {
    const options = (<HTMLSelectElement>document.querySelector('#menubox')).selectedOptions
    for (let i = 0; i < options.length; ++i) {
      let split = options[i].value.split('-');
      if (isNaN(parseFloat(split[1]))) continue;
      let item = new OrderItem(new MenuItem(true, split[0], parseFloat(split[1]), null), 1);
      let contains : boolean = false;
      for (let thing of this.curOrders) {
        if (thing.getName() === item.getName()) {
          contains = true;
          break;
        }
      }
      if (!contains) this.curOrders.push(item);
    }
  }

  removeItem(item : OrderItem) : void {
    this.curOrders = this.curOrders.filter(it => {
      return it.getName() !== item.getName()
    })
  }

  getCurTotal() : number {
    let total = 0
    this.curOrders.forEach(item => {
      total += item.getTotalPrice()
    })
    return total
  }

  removeAll() : void {
    this.curOrders = []
  }
  
  addToCart() : void {
    let store = this.store.clone();
    let order = {'store': store, 'orders': this.curOrders};
    localStorage.setItem(`cart-add-${this.index}-${Date.now()}`, JSON.stringify(order));
    this.toggleShow();
    this.removeAll();
  }
}
