import { Component, OnInit, Input } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { OrderInfo } from '../OrderInfo';

@Component({
  selector: 'app-rating-stars',
  templateUrl: './rating-stars.component.html',
  styleUrls: ['./rating-stars.component.css']
})
export class RatingStarsComponent implements OnInit {
  @Input() orderInfo: OrderInfo;

  constructor() { }

  ngOnInit() {
  }

  getRating(): number {
    let total: number = 0
    let radio: NodeListOf<HTMLInputElement> = <NodeListOf<HTMLInputElement>>document.querySelectorAll('input[type="radio"]')
    for (let i = 0; i < radio.length; ++i) {
      if (radio[i].checked) total += parseFloat(radio[i].value)
    }

    if (total < 0) total = 0;
    if (total > 5) total = 5;
    return total;
  }

  submitRating(): void {
    const rating = this.getRating();
    const api_url = 'api/' + (AppComponent.isDelivery ? 'Customer' : 'Delivery') + '/do_rank/';

    const fetch_body = AppComponent.isDelivery ? 
      {'customer_id': this.orderInfo.customerId, 'rank': rating} : 
      {'delivery_id': this.orderInfo.deliveryStaffObj.customerId, 'rank': rating};

    console.log(fetch_body);

    fetch(AppComponent.API_URL + api_url, {
      mode: 'cors',
      credentials: 'include',
      method: 'POST',
      body: JSON.stringify(fetch_body),
      headers: { 'content-type': 'application/json' }
    }).then(res => res.json()).then(data => {
      if (!data['status']) console.log(data['msg']);
    });
  }
}
