import { StoreInfo } from './StoreInfo';
import { MenuItem } from './MenuInfo';
import { AppComponent } from '../app.component';
import { CustomerInfo } from './UserInfo';

enum States {
    WAITING,
    GOING,
    PENDING_DELIVERY,
    PENDING_USER,
    DONE,
    ERROR
}

const StatesDecoder = {
    0: States.WAITING,
    1: States.GOING,
    2: States.PENDING_DELIVERY,
    3: States.PENDING_USER,
    4: States.DONE,
    5: States.ERROR
}

export class OrderInfo {
    private prePrice : number = 0
    private isReadOnly = false;
    constructor(
        public menuItems : Map<StoreInfo, OrderItem[]> = new Map<StoreInfo, OrderItem[]>(),
        public orderState : States = States.WAITING,
        public deliveryStaff : string = undefined,
        public orderId : string = Date.now().toString() + Math.floor(Math.random() * 100),
        public profit : number = 0,
        public distance : number = 0,
        public location: string = '',
        public position: object = {lat: 25.15, lng: 121.77},
        public deliveryStaffObj: CustomerInfo = null,
        public customerId: string = null,
        public customerObj: CustomerInfo = null
    ) { }

    addToOrder(store : StoreInfo, item : OrderItem) : void {
        if (this.menuItems.has(store)) {
            this.menuItems.get(store).push(item)
        } else {
            this.menuItems.set(store, [item])
        }
    }

    getKeys() : StoreInfo[] {
        return [...this.menuItems.keys()]
    }

    getValByKey(store : StoreInfo) : OrderItem[] {
        return this.menuItems.get(store)
    }

    removeItem(storeInfo : StoreInfo, item : OrderItem) : void {
        if (!this.menuItems.has(storeInfo)) return;
        this.menuItems.set(storeInfo, this.menuItems.get(storeInfo).filter(function(ele, index, array){
            return ele !== item;
        }));
        let toDelete: string;
        for (let i = 0; i < localStorage.length; ++i) {
            if (storeInfo.work_uid === JSON.parse(localStorage.getItem(localStorage.key(i))).store.work_uid) {
                toDelete = localStorage.key(i);
            }
        }
        localStorage.removeItem(toDelete);
        localStorage.setItem(toDelete, JSON.stringify({store: storeInfo, orders: this.menuItems.get(storeInfo)}));
    }

    getTotalCost() : number {
        let totalCost : number = 0;
        for (let key of this.getKeys()) {
            for (let item of this.getValByKey(key)) {
            totalCost += item.getTotalPrice()
            }
        }
        return totalCost
    }

    getDeliveryProfit() {
        let totalPrice = this.prePrice = this.getTotalCost()
        if (totalPrice === 0 || this.distance === 0) {
            this.profit = 0
            return
        }
        fetch(AppComponent.API_URL +
        `api/PriceCount/countPrice/?foodprice=${totalPrice}&distance=${this.distance}`, {
            method:'GET'
        }).then(res => res.json()).then(res => {
            this.profit = parseInt(res)
        })
    }

    getFinalPrice() : number {
        let total = this.getTotalCost()
        if (this.isReadOnly) return this.profit + total
        if (this.prePrice !== total) this.getDeliveryProfit()
        return this.profit + total
    }

    setDelivery(staff: string): void {
        this.deliveryStaff = staff;
        this.going();
    }

    finish() : void { this.orderState = States.DONE }
    going() : void { this.orderState = States.GOING }
    error() : void { this.orderState = States.ERROR }
    waiting() : void { this.orderState = States.WAITING }
    pending(): void { this.orderState = AppComponent.isDelivery ? States.PENDING_USER : States.PENDING_DELIVERY }
    isFinished() : boolean { return this.orderState === States.DONE }
    isGoing() : boolean { return this.orderState === States.GOING }
    isGoingOrPendingMe() : boolean { return this.isGoing() || ((AppComponent.isDelivery) ? this.isPendingDelivery() : this.isPendingUser()) }
    isError() : boolean { return this.orderState === States.ERROR }
    isWaiting(): boolean { return this.orderState === States.WAITING }
    isPending(): boolean { return this.isPendingUser() || this.isPendingDelivery() }
    isPendingUser(): boolean { return this.orderState === States.PENDING_USER }
    isPendingDelivery(): boolean { return this.orderState === States.PENDING_DELIVERY }
    isGoingOrWaitingOrPending(): boolean { return this.isGoing() || this.isWaiting() || this.isPending() }

    clone(newDate : string = undefined) : OrderInfo {
        if (newDate === undefined) return new OrderInfo(new Map<StoreInfo, OrderItem[]>(this.menuItems), this.orderState, this.deliveryStaff, undefined, this.profit, this.distance, this.location, Object.create(this.position))
        return new OrderInfo(new Map<StoreInfo, OrderItem[]>(this.menuItems), this.orderState, this.deliveryStaff, newDate, this.profit, this.distance, this.location, Object.create(this.position))
    }

    toString() : string {
        let str = `訂單編號：${this.orderId}\n外送員：${this.deliveryStaff}\n總金額：${this.getFinalPrice()}\n外送地址：${this.location}`
        return str;
    }

    toStringPromise(): Promise<string> {
        return new Promise<string>((resolve) => {
            let timer = setInterval(() => {
                if (this.customerObj && this.deliveryStaffObj) {
                    clearInterval(timer);
                    resolve(
                        `訂單編號：${this.orderId}\n外送員：${this.deliveryStaff} (電話：${this.deliveryStaffObj.phoneNumber})\n食客：${this.customerObj.userName} (電話：${this.customerObj.phoneNumber})\n總金額：${this.getFinalPrice()}\n外送地址：${this.location}`
                    );
                }
            }, 200);
        });
    }

    stateToString() : string {
        if (this.isFinished()) return '已完成'
        if (this.isGoing()) return '外送中'
        if (this.isPending()) return '等待完成'
        if (this.isWaiting()) return '等待外送員'
        if (this.isError()) return '已刪除'
        return ''
    }

    getJson(): object {
        let menuItems = [];
        for (let item of this.menuItems.keys()) {
            let menuItem = {};
            menuItem['store'] = item;
            menuItem['orders'] = this.menuItems.get(item);
            menuItems.push(menuItem);
        }
        return {
            orderState: this.orderState,
            deliveryStaff: this.deliveryStaff,
            menuItems,
            profit: this.profit,
            distance: this.distance,
            location: this.location,
            position: this.position
        };
    }
    
    static createFromObj(obj: any): OrderInfo {
        let itemMap = new Map<StoreInfo, OrderItem[]>();
        let newOrder = new OrderInfo();

        let storeOrderObj = obj['order_items'];
        for (let storeOrder of storeOrderObj['menuItems']) {
            let storeObj = storeOrder['store'];
            let store = new StoreInfo(
                storeObj['storeName'],
                storeObj['phone'],
                storeObj['location'],
                storeObj['principal'],
                null,
                null,
                null,
                storeObj['position']
            );
    
            let menuItems = [];
            for (let item of storeOrder['orders']) {
                let menuItemObj = item['menuItem'];
                let order = new OrderItem(
                    new MenuItem(true, menuItemObj['productName'], menuItemObj['price'], null),
                    item['count']
                );
                menuItems.push(order);
            }
            itemMap.set(store, menuItems);
        }

        newOrder.orderId = obj['order_id'];
        newOrder.menuItems = itemMap;
        newOrder.location = storeOrderObj['location'];
        newOrder.position = storeOrderObj['position'];
        newOrder.orderState = StatesDecoder[obj['status']];
        newOrder.profit = storeOrderObj['profit'];
        newOrder.isReadOnly = true;
        newOrder.deliveryStaff = obj['delivery_id'];
        newOrder.customerId = obj['customer_id'];

        fetch(AppComponent.API_URL + `api/Customer/${newOrder.customerId}/`, {method: 'GET'})
        .then(res => res.json()).then(data => {
            newOrder.customerObj = CustomerInfo.createFromJsonObj(data);
        });

        return newOrder;
    }
}

export class OrderItem {
    menuItem : MenuItem
    count : number
    
    constructor(menuItem : MenuItem, count : number) {
        this.menuItem = menuItem
        this.count = count
    }

    getSinglePrice() : number {
        return this.menuItem.price
    }

    getTotalPrice() : number {
        return this.menuItem.price * this.count
    }

    getName() : string {
        return this.menuItem.productName
    }

    addCount() : void {
        this.count += 1;
    }

    minusCount() : void {
        if (this.count === 1) return;
        this.count -= 1;
    }
}