export class CustomerInfo {
    constructor(
        public customerId: string,
        public userName: string,
        public nickName: string,
        public password: string,
        public phoneNumber: string,
        public address: string,
        public gender: string,
        public mailAddress: string,
        public deliveryMode: boolean,
        public discribe: string,
        public otherContact: object,
        public customerRank: number,
        public customerRankTimes: number,
        public isValid: boolean
    ) {}

    toFormatJson(): object {
        return {
            mail_address: this.mailAddress,
            psw: this.password,
            user_name: this.userName,
            nick_name: this.nickName,
            gender: this.gender,
            phone_number: this.phoneNumber,
            address: this.address,
            discribe: this.discribe,
            other_contact: this.otherContact,
            delivery_mode: this.deliveryMode,
            custumer_rank: this.customerRank,
            custumer_rank_times: this.customerRankTimes,
            customer_id: this.customerId
        };
    }

    static createFromJsonObj(obj: object): CustomerInfo {
        return new CustomerInfo(
            obj['customer_id'],
            obj['user_name'],
            obj['nick_name'],
            obj['psw'],
            obj['phone_number'],
            obj['address'],
            obj['gender'],
            obj['mail_address'],
            obj['delivery_mode'],
            obj['discribe'],
            obj['other_contact'],
            obj['custumer_rank'],
            obj['custumer_rank_times'],
            obj['is_valid']
        );
    }
}