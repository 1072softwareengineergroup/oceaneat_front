export class MenuInfo {
    menu : MenuItem[]
    private typeMenu : object

    constructor() {
        this.menu = []
        this.typeMenu = {}
    }

    getMenuObject() : object {
        this.typeMenu = {}
        this.menu.forEach(item => {
            if (this.typeMenu[item.classType] === undefined) this.typeMenu[item.classType] = []
            this.typeMenu[item.classType].push(item)
        });
        return this.typeMenu
    }
}

export class MenuItem {
    isEnable : boolean
    productName : string
    price : number
    picture : string
    classType : string

    constructor(isEnable : boolean, productName : string, price : number, picture : string, classType : string = '-') {
        this.isEnable = isEnable
        this.productName = productName
        this.price = price
        this.picture = picture
        this.classType = classType
    }
}