import { Component, OnInit, AfterViewInit } from '@angular/core';

import * as p5 from 'p5';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {
  private yoff = 0.0;
  VersionNumber = AppComponent.VersionNumber;
  hovering = false;

  constructor() { }

  ngOnInit() {
  }

  searchTextChange(ev: KeyboardEvent) {
    if (ev.keyCode === 13) this.search();
    this.hovering = false;
    const word = (<HTMLInputElement>document.querySelector('#searchInput')).value;
    if (word.trim() === '') return;
    this.hovering = true;
  }

  search() : void {
    let word = (<HTMLInputElement>document.querySelector('#searchInput')).value
    if (word !== '') {
      window.open('/search?s=' + word, '_self')
      return;
    }
    window.open('/search', '_self')
  }
  
  ngAfterViewInit() {
    this.startP5();
  }

  startP5() {
    let sketch = (p : any) => { this.makeCanvas(p, p.windowWidth, 244, 0) };
    new p5(sketch, document.getElementById('wave'));
  }

  makeCanvas(p : any, w : number, h : number, bias : number) : void {
    p.setup = () => {
      p.createCanvas(w - .5, h);
    };
  
    p.draw = () => {
      // p.background(176, 184, 189);
      p.clear();
      p.fill(0,119,200);
      p.noStroke();
      // We are going to draw a polygon out of the wave points
      p.beginShape();
  
      let xoff = 0; // Option #1: 2D Noise
      // let xoff = yoff; // Option #2: 1D Noise
  
      // Iterate over horizontal pixels
      for (let x = 0; x <= p.windowWidth - .5 + 5; x += 10) {
        // Calculate a y value according to noise, map to
  
        // Option #1: 2D Noise
        let y = p.map(p.noise(xoff, this.yoff), 0, 1, 50, 200);
  
        // Option #2: 1D Noise
        // let y = map(noise(xoff), 0, 1, 200,300);
  
        // Set the vertex
        p.vertex(x, y);
        // Increment x dimension for noise
        xoff += 0.05;
      }
      // increment y dimension for noise
      this.yoff += 0.01;
      p.vertex(p.windowWidth - .5 + 5, h);
      p.vertex(0, h);
      p.endShape(p.CLOSE);
    }

    p.windowResized = () => {
      p.clear();
      p.resizeCanvas(p.windowWidth - .5 - bias, h);
    }
  }

}
