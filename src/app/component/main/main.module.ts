import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { HeadbarModule } from '../headbar/headbar.module';

@NgModule({
    imports: [
        CommonModule,
        MainRoutingModule,
        HeadbarModule
    ],
    declarations: [MainComponent]
})
export class MainModule { }