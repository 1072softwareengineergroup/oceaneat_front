import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { OrderDisplayComponent } from './order-display.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        OrderDisplayComponent
    ],
    exports: [
        OrderDisplayComponent
    ]
})
export class OrderDisplayModule { }