import { Component, OnInit, Input } from '@angular/core';
import { OrderInfo } from '../OrderInfo';

@Component({
  selector: 'app-order-display',
  templateUrl: './order-display.component.html',
  styleUrls: ['./order-display.component.css']
})
export class OrderDisplayComponent implements OnInit {
  @Input() orderInfo : OrderInfo
  @Input() isCart : boolean

  constructor() { }

  ngOnInit() {
  }

}
