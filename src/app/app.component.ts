import { Component, OnInit } from '@angular/core';
import { CustomerInfo } from './component/UserInfo';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  static API_URL: string;
  static readonly VAPID_PUBLIC_KEY = 'BFecJLI3KQfgioH1_lVJkdJKP9Gcw_jKzGbdi-e3_-lTUwmghLw7RdKxrxH8mC1Kk24J85JvHrS1zAz58GhwCi4';
  static VersionNumber = `Version: ${new Date(1561094572239).toLocaleString()}`;
  static CallAfterInit: Function[] = [];
  static UserObj: CustomerInfo = null;
  static User: Promise<CustomerInfo>;

  ngOnInit() {
    AppComponent.API_URL = window.location.hostname === 'localhost' ? 'http://localhost:8000/' : 'https://nlp7.cse.ntou.edu.tw:8000/';
    AppComponent.User = new Promise<CustomerInfo>((resolve, reject) => {
      let time = 0;
      if (AppComponent.UserObj) resolve(AppComponent.UserObj);
      else {
        setInterval(() => {
          if (AppComponent.UserObj) resolve(AppComponent.UserObj);
          ++time;
          if (time === 10) {
            resolve(null);
          }
        }, 200);
      }
    });
    AppComponent.testLogin();

    if ('serviceWorker' in navigator && window.location.hostname !== 'localhost') {
        navigator.serviceWorker.register('./ngsw-worker.js');
    }
  }

  static get isLogin(): boolean {
    return !isNaN(parseInt(sessionStorage['userId']));
  }

  static get isCustomer(): boolean {
    return this.userType === 0 && AppComponent.isLogin;
  }

  static get isDelivery(): boolean {
    return this.userType === 1 && AppComponent.isLogin;
  }

  static get isRestaurant(): boolean {
    return this.userType === 2 && AppComponent.isLogin;
  }

  static get isQualified(): boolean {
    return (AppComponent.UserObj ? JSON.stringify(AppComponent.UserObj.otherContact) !== '{}' : false);
  }

  static get UserId(): string {
    if (sessionStorage['userId'] === undefined) return null;
    return sessionStorage['userId'];
  }

  static get CurLocation(): object {
    if (localStorage['curLocation'] === undefined) return null;
    return JSON.parse(localStorage['curLocation']);
  }

  static testLogin(rederict = false): void {
    fetch(AppComponent.API_URL + 'is_login/', {
      mode: 'cors',
      credentials: 'include',
      method: 'GET'
    })
    .then(res => {
      if (!res.ok) throw Error('not login');
      return res;
    })
    .then(res => res.json())
    .then(data => {
      if (data['status']) {
        sessionStorage['userId'] = data['user_id'];
        sessionStorage['userType'] = AppComponent.UserTypeDecoder[data['user_type']];
        AppComponent.getUserObj();
      }
      else if (sessionStorage['userId']) sessionStorage.removeItem('userId');

      AppComponent.excute();

      if (rederict) {
        if (this.isRestaurant) window.open('/management', '_self');
        else window.open('/', '_self');
      }
    })
    .catch(err => {
      if (sessionStorage['userId']) sessionStorage.removeItem('userId');
      AppComponent.excute();
    });
  }

  static getUserObj() {
    fetch(AppComponent.API_URL + `api/Customer/${AppComponent.UserId}/`, {
      method: 'GET'
    }).then(res => res.json()).then(data => { 
      AppComponent.UserObj = CustomerInfo.createFromJsonObj(data);
    });
  }

  static get userType(): number {
    if (sessionStorage['userType'] === undefined) return null;
    return parseInt(sessionStorage['userType']);
  }

  static UserTypeDecoder = {
    'customer': 0,
    'delivery': 1,
    'restaurant manager': 2
  }

  static excute() {
    for (const func of AppComponent.CallAfterInit) func();
  }
}
